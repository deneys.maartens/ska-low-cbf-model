PROJECT = low-cbf-model

# Fixed variables
# Timeout for gitlab-runner when run locally
TIMEOUT = 86400

CI_PROJECT_DIR ?= .
CI_PROJECT_PATH_SLUG ?= low-cbf-model
CI_ENVIRONMENT_SLUG ?= low-cbf-model

# We're using a non-standard directory structure (for now)
PYTHON_VARS_BEFORE_PYTEST ?= PYTHONPATH=./python
PYTHON_TEST_FILE ?= python/tests/ 
PYTHON_LINT_TARGET ?= python/ 
PYTHON_SWITCHES_FOR_PYLINT=--fail-under=8

# define private overrides for above variables in here
-include PrivateRules.mak

# Include the required modules from the SKA makefile submodule
include .make/docs.mk
include .make/help.mk
include .make/python.mk
include .make/make.mk
include .make/release.mk
