function [CORreg, CORconst] = getCORRegisterSettings(arrayConfigFull,modelConfig)
%
% Generate the register settings in the PST firmware from the LMC configuration
% Returns:
%  CORreg - Array of register settings. Each entry is a struct with fields
%     "name"   : ARGs name of the register, used to upload it to the firmware.
%     "offset" : Address offset from the start address of the register. Used for memories where only part of the memory is to be loaded.
%     "value"  : Value to be loaded into the register. For memories, this can be an array. 
%                If there are several updates to the register settings, then they are included
%                as different columns. E.g. a 1024x4 array would be 4 updates of a 1024 deep memory.
%  CORConst - list of constants used for the processing. These are built into ROMs or are default register values in the firmware.
% ------------------------------------------------------------------------------
% CORreg
% ------
%
% There is a field for each register setting. These are named according to the module and register name in the firmware.
% Also see the module level documentation on confluence.
% In some cases the register settings can change over time. To accomodate this, revised register settings are included
% in the output structure in the column index. e.g. localDoppler.startPhase refers to a block of 1024 registers. If
% there are 4 sets of values, then localDoppler.startPhase will be a 1024x4 matrix.
%
% Registers set here:
%  lfaadecode100g.statctrl.vctable        : maximum 1024 entry virtual channel table
%  lfaadecode100g.statctrl.total_stations : total number of stations
%  lfaadecode100g.statctrl.total_coarse   : total number of coarse channels
%  lfaadecode100g.statctrl.total_channels : total number of virtual channels. Should be total_stations * total_coarse.
%  
%  ct_atomic_pst_in.config.table_0  :  Configures the coarse and fine delays.
%  ct_atomic_pst_in.config.table_1  :
%  
%  
% ---------------------------------------------------------------------------------
% CORconst
% --------
%
%  filterbanks.firtaps.data
%
%
%  

%% Registers for the filterbanks.
% The filter taps for the filterbanks have default values in the firmware, so they are stored in CORconst instead of CORreg.
% generate_MaxFlt creates filters with a DC response of 0.5
% i.e. sum(globalreg.PSSFilterbankTaps(X:64:end)) = 0.5
% The maximum value for the filter taps is about 0.55
% The firmware has 18 bits to represent the filter taps (i.e. range = -131072 to 131071).
% So scale by 2^17 (=131072)
%
% The generate_MaxFlt function doesn't work in Octave, so the data is saved to a file instead.
% The function "write_filter_taps.m" can be run in Matlab to generate the files
%  pss-filter.txt, pst-filter.txt, and correlator-filter.txt
% Note that there is an equivalent piece of code in the firmware repository :
%  low-cbf-firmware/libraries/signalProcessing/filterbanks/src/matlab/get_row_coefficients
% which generates the .coe files to initialise the memories used in the firmware for the filterbanks.
%
%
% globalreg.PSSFilterbankTaps = load('pss-filter.txt');
% globalreg.correlatorFilterbankTaps = load('correlator-filter.txt'); % Correlator, 4096 point FFT, 12 taps.
CORconst.CORFilterbankTaps = load('correlator-filter.txt'); % PST, 4096 point FFT, 12 taps

% Also find the location of the maximum of the filters, which is used for setting the fine delay (since fine delay changes with time).
mv = max(CORconst.CORFilterbankTaps);
mi = find(CORconst.CORFilterbankTaps == mv);
CORconst.CORFilterbankMaxIndex = median(mi);


%% Work out how many delay updates we need
if (modelConfig.delayUpdatePeriod == 0)
    updates = 1; % i.e. just the initial value.
else
    totalRuntime = modelConfig.runtime * 1080e-9 * 408 * 2048; 
    updates = floor(totalRuntime / modelConfig.delayUpdatePeriod) + 1;
    % Number of LFAA frames per update period
    updatePeriod = (totalRuntime / updates) / (1080e-9 * 2048);
end

registerCount = 0;  % Total number of different registers we have set.

%% Registers for LFAASPEADDecoder module
% .virtualChannel
% Each entry in the virtual channel table has substation_id, station_id,
% beam_id, frequency_id, subarray_id.

virtualChannelTable = zeros(1,1024,13); % First index is for multiple virtual channel tables (i.e. one per 100GE interface).
demapTable = zeros(1,256,7); % virtual channel demapping table in the second corner turn.
subarrayBeamTable = zeros(1,256,7);

current_virtualChannel = 0;
virtualChannelCount = 0;
current_HBM_base = [0 0];  % base address for 2nd corner turn data store for each correlator cell.
% Where we are currently up to in the subarray-beam table. 2 entries, one for each correlator cell.
% Second correlator cell starts at entry 128.
current_SB_index = [0 128]; 

% This assumes that there is only one correlator alveo being created by
% this test case. To do : determine the number of correlator Alveos that
% are required based on the number of correlations, stations, and
% bandwidth.
% Doing this properly would also require working out how to allocate correlations and frequency channels across multiple Alveos.
% (instead of just allocating them in order they are specified as done here).
correlator_core_idx = 1;

for correlation = 1:length(arrayConfigFull.correlator)
    currentSubarray = arrayConfigFull.correlator(correlation).subArray;
    currentBeam = arrayConfigFull.correlator(correlation).stationBeam;
    
    % Create an entry in the subarray-beam table
    
    if (arrayConfigFull.correlator(correlation).coarseStart < modelConfig.channelList(1))
        SB_firstCoarse = modelConfig.channelList(1);
        SB_firstFine = 0;
    else
        SB_firstCoarse = arrayConfigFull.correlator(correlation).coarseStart;
        SB_firstFine = arrayConfigFull.correlator(correlation).fineStart;
    end
    % The last coarse channel according to the configuration of the
    % correlator
    coarseEnd = arrayConfigFull.correlator(correlation).coarseStart + floor(arrayConfigFull.correlator(correlation).NFine/3456);
    if (coarseEnd > modelConfig.channelList(end)) 
        % channelList(end) is the last coarse channel; if this was the same as the first coarse channel,
        % then we should have 3456 fine channels, hence the +3456
        SB_NFine = (modelConfig.channelList(end)*3456 + 3456) - (SB_firstCoarse*3456 + SB_firstFine);
    else
        % end frequency channel specified for the correlation was within the channels being processed in modelConfig.channelList, 
        % so NFine is end specified for correlation minus wherever we decided to start from.
        SB_NFine = (arrayConfigFull.correlator(correlation).coarseStart*3456 + arrayConfigFull.correlator(correlation).NFine) - (SB_firstCoarse*3456 + SB_firstFine);
    end
    
    this_SB_index = current_SB_index(arrayConfigFull.correlator(correlation).correlatorCell + 1);
    current_SB_index(arrayConfigFull.correlator(correlation).correlatorCell + 1) = current_SB_index(arrayConfigFull.correlator(correlation).correlatorCell + 1) + 1;
    subarrayBeamTable(correlator_core_idx,this_SB_index+1,1) = length(arrayConfigFull.subArray(arrayConfigFull.correlator(correlation).subArray).logicalIDs); % Number of stations in this subarray
    subarrayBeamTable(correlator_core_idx,this_SB_index+1,2) = SB_firstCoarse; % Starting coarse frequency channel
    subarrayBeamTable(correlator_core_idx,this_SB_index+1,3) = SB_firstFine; % Starting fine frequency channel
    subarrayBeamTable(correlator_core_idx,this_SB_index+1,4) = SB_NFine; % Number of fine frequency channels to store
    subarrayBeamTable(correlator_core_idx,this_SB_index+1,5) = arrayConfigFull.correlator(correlation).fineIntegration; % fine channels per integration
    subarrayBeamTable(correlator_core_idx,this_SB_index+1,6) = arrayConfigFull.correlator(correlation).timeIntegration; % time samples per integration
    subarrayBeamTable(correlator_core_idx,this_SB_index+1,7) = current_HBM_base(arrayConfigFull.correlator(correlation).correlatorCell + 1); % HBM base address for this data.
    
    % Add the amount of memory needed in the second corner turn for this correlation
    %  = (512 bytes) * ceil(number of stations / 4) * (6 time sample blocks) * (number of fine channels)
    current_HBM_base(arrayConfigFull.correlator(correlation).correlatorCell + 1) = current_HBM_base(arrayConfigFull.correlator(correlation).correlatorCell + 1) + 512 * ceil(subarrayBeamTable(correlator_core_idx,correlation,1)/4) * 6 * SB_NFine;
    
    if (current_HBM_base(arrayConfigFull.correlator(correlation).correlatorCell + 1) > (1.5*2^30))
        error('Exceeded the 1.5Gbyte limit for second corner turn buffer')
    end
    
    % Create entries in the virtual channel table
    for channel = 1:length(modelConfig.channelList)
        %for station = 0:(length(arrayConfigFull.stations(1).stationIDs) - 1)
        for station_logicalID_index = 1:length(arrayConfigFull.subArray(arrayConfigFull.correlator(correlation).subArray).logicalIDs)
            station_logicalID = arrayConfigFull.subArray(currentSubarray).logicalIDs(station_logicalID_index);
            stationIndex = find(arrayConfigFull.stationsFull.logicalIDs == station_logicalID);
            
            currentStation = arrayConfigFull.stationsFull.stationIDs(stationIndex);
            currentSubstation = arrayConfigFull.stationsFull.substationIDs(stationIndex);
            
            % Columns in virtualChannelTable
            %  1 => frequency_id, 2 => beam_id, 3 => substation_id, 4 => subarray_id, 5 => station_id, 
            %  6 => index X into arrayConfigFull.stationBeam(X), 7 => index X into arrayConfigFull.subArray(X), 8 => logical ID, 9 => table entry is unused
            % 10 => LRU this stationID came in on.
            % 11 => Virtual Channel.
            virtualChannelCount = virtualChannelCount + 1;

            % These entries get combined to form the virtual channel table in the lfaadecode100g module
            virtualChannelTable(correlator_core_idx,virtualChannelCount,1) = modelConfig.channelList(channel);  % The sky frequency in the LFAA packet header, (sky frequency = this value * 781.25 kHz)
            virtualChannelTable(correlator_core_idx,virtualChannelCount,2) = arrayConfigFull.stationBeam(currentBeam).index;  % The station Beam in the LFAA packet header
            virtualChannelTable(correlator_core_idx,virtualChannelCount,3) = currentSubstation; % The substation in the LFAA packet header
            virtualChannelTable(correlator_core_idx,virtualChannelCount,4) = currentSubarray;   % The subarray in the LFAA packet header
            virtualChannelTable(correlator_core_idx,virtualChannelCount,5) = currentStation;   % The station in the LFAA packet header
            % The remaining entries are for convenience use later in this function.
            virtualChannelTable(correlator_core_idx,virtualChannelCount,6) = currentBeam;  % Just for convenience; used later in this function to look up the correct stationBeam parameters for this entry.
            virtualChannelTable(correlator_core_idx,virtualChannelCount,7) = currentSubarray; %subArrayIndexes(subArray);  % Also just for convenience; used to look up the subarray this virtual channel is part of.
            virtualChannelTable(correlator_core_idx,virtualChannelCount,8) = station_logicalID;   % logical ID of this station, can differ from the station id when there are substations.
            virtualChannelTable(correlator_core_idx,virtualChannelCount,9) = 0; % this entry is not invalid.
            virtualChannelTable(correlator_core_idx,virtualChannelCount,10) = 1;  % Unused.
            virtualChannelTable(correlator_core_idx,virtualChannelCount,11) = virtualChannelCount-1;
            virtualChannelTable(correlator_core_idx,virtualChannelCount,12) = channel;  % record the channel count and station count for this virtual channel
            virtualChannelTable(correlator_core_idx,virtualChannelCount,13) = station_logicalID_index;
            virtualChannelTable(correlator_core_idx,virtualChannelCount,14) = current_virtualChannel;
            max_virtualChannel = current_virtualChannel;
            % Also put an entry in the demap table for this virtual channel
            % One entry for every 4th station.
            if (mod(current_virtualChannel,4) == 0)
                demapTable(correlator_core_idx,current_virtualChannel/4 + 1,1) = this_SB_index;  % index into the subarray-beam table
                demapTable(correlator_core_idx,current_virtualChannel/4 + 1,2) = station_logicalID_index - 1;  % station within this subarray
                demapTable(correlator_core_idx,current_virtualChannel/4 + 1,3) = modelConfig.channelList(channel); % Frequency channel
                demapTable(correlator_core_idx,current_virtualChannel/4 + 1,4) = 1; % valid;
                demapTable(correlator_core_idx,current_virtualChannel/4 + 1,5) = 0; % start fine channel to forward
                demapTable(correlator_core_idx,current_virtualChannel/4 + 1,6) = 0; % end fine channel to forward
                demapTable(correlator_core_idx,current_virtualChannel/4 + 1,7) = 0; % destination tag for forwarding of fine channels
            end
            
            current_virtualChannel = current_virtualChannel + 1;
        end
        % Within a group of stations with the same subarray, beam, and
        % frequency, virtual channel must be in blocks of 4, with virtual
        % channel numbers skipped if need be to ensure that they start on a
        % multiple of 4 boundary.
        if (mod(current_virtualChannel,4) ~= 0)
            current_virtualChannel = 4*ceil(current_virtualChannel/4);
        end
    end
end

%% Write out the virtual channel table
virtualChannelTemp = ones(4096,1,'uint32')*(2^31);
for vc = 1:virtualChannelCount
    % Pack into 32 bit words as used in the table. 
    %  First word : 
    %   bits 2:0   = substation_id, 
    %   bits 12:3  = station_id,    
    %   bits 16:13 = beam_id, 
    %   bits 25:17 = frequency_id,
    %   bits 30:26 = subarray_id, 
    %   bit  31    = set to '1' to indicate this entry is invalid 
    %  Second Word : 
    %   bits (9:0) =  Virtual channel.  
    virtualChannelTemp(2*(vc-1) + 1) = uint32(virtualChannelTable(correlator_core_idx,vc,3) + ...
                                    virtualChannelTable(correlator_core_idx,vc,5) * 2^3 + ...
                                    virtualChannelTable(correlator_core_idx,vc,2) * 2^13 + ...
                                    virtualChannelTable(correlator_core_idx,vc,1) * 2^17 + ...
                                    virtualChannelTable(correlator_core_idx,vc,4) * 2^26);
    % The actual virtual channel assigned to this station, channel, beam
    virtualChannelTemp(2*(vc-1) + 2) = virtualChannelTable(correlator_core_idx,vc,14); 
end
registerCount = registerCount + 1;
CORreg(registerCount).name = 'lfaadecode100g.statctrl.vctable';
CORreg(registerCount).offset = 0;
CORreg(registerCount).value = virtualChannelTemp;

%% Write out the demap table
demapTemp = zeros(1024,1,'uint32');
for dm = 1:256
    % Only populates the first buffer (The full table is double buffered).
    demapTemp(2*(dm-1)+1) = demapTable(correlator_core_idx,dm,1) + ...
                        demapTable(correlator_core_idx,dm,2) * 2^8 + ...
                        demapTable(correlator_core_idx,dm,3) * 2^20 + ...
                        demapTable(correlator_core_idx,dm,4) * 2^31;
    demapTemp(2*(dm-1)+2) = demapTable(correlator_core_idx,dm,5) + ...
                        demapTable(correlator_core_idx,dm,6) * 2^12 + ...
                        demapTable(correlator_core_idx,dm,7) * 2^24;
end
registerCount = registerCount + 1;
CORreg(registerCount).name = 'corr_ct2.statctrl.VC_demap';
CORreg(registerCount).offset = 0;
CORreg(registerCount).value = demapTemp;

%% Write out the subarray-beam table
SBTemp = zeros(2048,1,'uint32');
for SB = 1:256
    % subarray beam table is packed with
    % word 0 : bits 15:0 = number of stations in this subarray-beam
    %          bits 31:16 = starting coarse frequency channel, \
    % word 1 : bits (15:0) = starting fine frequency channel \
    % word 2 : bits (23:0) = Number of fine channels stored \
    %          bits (29:24) = Fine channels per integration \
    %          bits (31:30) = integration time; 0 = 283 ms, 1 = 849 ms, others invalid \
    % word 3 : bits (31:0) = Base Address in HBM within a 1.5 Gbyte block to store channelised source data for this subarray-beam 
    %
    if (subarrayBeamTable(correlator_core_idx,SB,6) == 192)
        integrationTime = 1;
    elseif (subarrayBeamTable(correlator_core_idx,SB,6) == 64)
        integrationTime = 0;
    elseif (subarrayBeamTable(correlator_core_idx,SB,6) == 0)
        % Should only occur if this table entry is unused.
        integrationTime = 0;
    else
        error('Integration time must be either 64 or 192 time samples'); 
    end
    
    SBTemp(4*(SB-1)+1) = subarrayBeamTable(correlator_core_idx,SB,1) + ...
                         subarrayBeamTable(correlator_core_idx,SB,2) * 2^16; 
    SBTemp(4*(SB-1)+2) = subarrayBeamTable(correlator_core_idx,SB,3);
    SBTemp(4*(SB-1)+3) = subarrayBeamTable(correlator_core_idx,SB,4) + ...
                         subarrayBeamTable(correlator_core_idx,SB,5) * 2^24 + ...
                         integrationTime * 2^30;
    SBTemp(4*(SB-1)+4) = subarrayBeamTable(correlator_core_idx,SB,7);
end
registerCount = registerCount + 1;
CORreg(registerCount).name = 'corr_ct2.statctrl.subarray_beam';
CORreg(registerCount).offset = 0;
CORreg(registerCount).value = SBTemp;


%% Other lfaadecode100g registers : table_select, total_channels
registerCount = registerCount + 1;
CORreg(registerCount).name = 'lfaadecode100g.statctrl.table_select';
CORreg(registerCount).offset = 0;
CORreg(registerCount).value = 0;

registerCount = registerCount + 1;
CORreg(registerCount).name = 'lfaadecode100g.statctrl.total_channels';
CORreg(registerCount).offset = 0;
CORreg(registerCount).value = current_virtualChannel;

%% Input Corner Turn
%  Delay for each coarse channel, based on the virtual channel number.
%  
%  The table in the corner turn has 2 x(32bit words) per virtual channel. 
%  Each pair of words has the following fields:
%    word 0 : bits 10:0 = coarse delay
%             bits 31:16 = H pol delta phase = phase step across the coarse channel (see confluence pages for details)
%    word 1 : bits 15:0 = V pol delta phase = phase step across the coarse channel 
%             bits 31:16 = delta delta phase (common to both polarisations)
%
%

registerCount = registerCount + 1;
CORreg(registerCount).name = 'corr_ct1.config.table_0';
CORreg(registerCount).offset = 0;
CORreg(registerCount).value = zeros(virtualChannelCount*4,ceil(updates/2));  % Every second update goes to table_0, every other update goes to table_1.

if(updates > 1)
    CORreg(registerCount+1).name = 'corr_ct1.config.table_1';
    CORreg(registerCount+1).offset = 0;
    CORreg(registerCount+1).value = zeros(virtualChannelCount*4,floor(updates/2));  % Every second update goes to table_0, every other update goes to table_1.
end

CORreg(registerCount+2).name = 'corr_ct1.config.table0_startpacket';
CORreg(registerCount+2).offset = 0;
CORreg(registerCount+2).value = 0;  % The packet count at which we start using the data in table_0

CORreg(registerCount+3).name = 'corr_ct1.config.table1_startpacket';
CORreg(registerCount+3).offset = 0;
CORreg(registerCount+3).value = 0;  % The packet count at which we start using the data in table_1

for frame = 1:updates
    if frame == 1
       CORreg(registerCount+2).value(frame) = 0;   % use table_0 from the start (i.e. frame 0)
       CORreg(registerCount+3).value(frame) = round(frame * updatePeriod);   % table_1 hasn't been loaded yet, but when it does, it will be used from this packet count.
    else
       CORreg(registerCount+2).value(frame) = round(floor((frame-1)/2)*2 * updatePeriod);        % i.e. 0, 0, 2, 2, 4, 4, 6, 6, etc.
       CORreg(registerCount+3).value(frame) = round((ceil((frame-1)/2)*2 - 1) * updatePeriod);   % i.e. 0, 1, 1, 
    end
end

% as currently only one station beam is available for all the pipelines, so actually for all the pipelines, all delay tables are same  
for virtualChannel = 0:max_virtualChannel
    % Find the index of this virtual channel in the virtual channel table.
    virtualChannel_index_full = find(virtualChannelTable(correlator_core_idx,:,14) == virtualChannel);
    % There can be gaps in the assigned virtual channels, so they may not all be used.
    if ~isempty(virtualChannel_index_full)
        virtualChannel_index = virtualChannel_index_full(1); % virtualChannel_index_full is a  list for virtualChannel=0, since that is the default value for unused entries in the table.
        % Get the delay polynomial for this virtual channel
        disp(['vc = ' num2str(virtualChannel) ', vc_index = ' num2str(virtualChannel_index)])
        logicalIDIndex = find(arrayConfigFull.subArray(virtualChannelTable(correlator_core_idx,virtualChannel_index,7)).logicalIDs == virtualChannelTable(correlator_core_idx,virtualChannel_index,8));

        delayPoly = arrayConfigFull.stationBeam(virtualChannelTable(correlator_core_idx,virtualChannel_index,6)).delayPolynomial(logicalIDIndex,:);  % The delay polynomial for this virtual channel 
        HVOffset  = arrayConfigFull.stationBeam(virtualChannelTable(correlator_core_idx,virtualChannel_index,6)).HVOffset(logicalIDIndex);

        for frame = 1:updates
            currentTime = (frame - 1) * 1080e-9 * 2048 * 408;
            % Evaluate the delay polynomial and its derivative
            delaySeconds = delayPoly(1) * currentTime^3 + delayPoly(2) * currentTime^2 + delayPoly(3) * currentTime + delayPoly(4);
            DdelayDt = 3 * delayPoly(1) * currentTime^2 + 2 * delayPoly(2) * currentTime + delayPoly(3);
            delaySamples = round(delaySeconds/(1080e-9));

            delayFracH = delaySeconds - delaySamples * 1080e-9; % in seconds
            delayFracV = delayFracH + HVOffset;
            % Fine delay uses the Phase change across one coarse channel.
            % See the coarse corner turn confluence page for an explanation.
            deltaPH = round((1/1080e-9) * (1/2) * delayFracH/(2^(-12)) * 2^3);  
            deltaPV = round((1/1080e-9) * (1/2) * delayFracV/(2^(-12)) * 2^3);
            deltadeltaP = round((1/1080e-9) * (1/2) * (DdelayDt * 64*1080e-9)/(2^(-15)) * 2^15);

            % The input corner turn also needs to include a phase offset and phase offset step per time
            skyFrequency = virtualChannelTable(correlator_core_idx,virtualChannel_index,1) * (27/32)*(1/1080e-9);  % sky frequency in Hz for the center of the band

            % To correctly implement the coarse delay, we need to include a phase offset.
            % Even if the required delay is an integer number of LFAA samples, we still need a phase offset 
            % because there is not an integer number of local oscillator clock periods in the 1080ns LFAA sample period.
            % (The non-integer relationship is cause by the 32/27 LFAA oversampling.)
            OffsetPhaseH = delaySeconds * skyFrequency;  % i.e. delay/skyPeriod. This value is in units of rotations (i.e. basePhase = 1 --> phase of 2*pi)
            OffsetPhaseV = (delaySeconds + HVOffset) * skyFrequency;

            offsetPH = mod(round(2^16 * mod(OffsetPhaseH,1)),2^16);
            offsetPV = mod(round(2^16 * mod(OffsetPhaseV,1)),2^16);

            deltaDelay = DdelayDt * 64 * 1080e-9;  % The delay offset is updated every 64 coarse channel time samples.
            phaseBaseTimeStep = deltaDelay * skyFrequency;
            offsetPStep = 2^32 * mod(phaseBaseTimeStep,1);

            if ((delaySamples < 1) || (delaySamples > 2047))
               error(['Calculated Coarse Delay is ' num2str(delaySamples) ' LFAA samples. It must be between 1 and 2047']);
            end
            % Two words in the table for each channel and station.
            % Updates alternate between "ct_atomic_pst_in.config.table_0" and "ct_atomic_pst_in.config.table_1"
            if(mod(frame,2) == 1)
              CORreg(registerCount).value(4*(virtualChannel) + 1,ceil(frame/2)) = 2^16 * TC16(deltaPH) + TC16(2048 - delaySamples);  % !!! Note : Large Delay means smaller read start address, hence (2048 - delaySamples)
              CORreg(registerCount).value(4*(virtualChannel) + 2,ceil(frame/2)) = 2^16 * TC16(deltadeltaP) + TC16(deltaPV);
              CORreg(registerCount).value(4*(virtualChannel) + 3,ceil(frame/2)) = 2^16 * (offsetPV) + (offsetPH);
              CORreg(registerCount).value(4*(virtualChannel) + 4,ceil(frame/2)) = mod(round(offsetPStep),2^32);
            else
              CORreg(registerCount+1).value(4*(virtualChannel) + 1,floor(frame/2)) = 2^16 * TC16(deltaPH) + TC16(2048 - delaySamples);
              CORreg(registerCount+1).value(4*(virtualChannel) + 2,floor(frame/2)) = 2^16 * TC16(deltadeltaP) + TC16(deltaPV);
              CORreg(registerCount+1).value(4*(virtualChannel) + 3,ceil(frame/2)) = 2^16 * (offsetPV) + (offsetPH);
              CORreg(registerCount+1).value(4*(virtualChannel) + 4,ceil(frame/2)) = mod(round(offsetPStep),2^32);
            end
        end
    end
end

registerCount = registerCount + 3;  % Since there are 3 extra registers set above.

% Reset the corner turn
registerCount = registerCount + 1;
CORreg(registerCount).name = 'corr_ct1.config.full_reset';
CORreg(registerCount).offset = 0;
CORreg(registerCount).value = 1;

registerCount = registerCount + 1;
CORreg(registerCount).name = 'corr_ct1.config.full_reset';
CORreg(registerCount).offset = 0;
CORreg(registerCount).value = 0;






