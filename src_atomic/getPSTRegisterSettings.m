function [PSTreg, PSTconst] = getPSTRegisterSettings(arrayConfigFull,modelConfig,num_PSTBF_cores)
%
% Generate the register settings in the PST firmware from the LMC configuration
% Returns:
%  PSTreg - Array of register settings. Each entry is a struct with fields
%     "name"   : ARGs name of the register, used to upload it to the firmware.
%     "offset" : Address offset from the start address of the register. Used for memories where only part of the memory is to be loaded.
%     "value"  : Value to be loaded into the register. For memories, this can be an array. 
%                If there are several updates to the register settings, then they are included
%                as different columns. E.g. a 1024x4 array would be 4 updates of a 1024 deep memory.
%  PSTConst - list of constants used for the processing. These are built into ROMs or are default register values in the firmware.
% ------------------------------------------------------------------------------
% PSTreg
% ------
%
% There is a field for each register setting. These are named according the module and register name in the firmware.
% Also see the module level documentation on confluence.
% In some cases the register settings can change over time. To accomodate this, revised register settings are included
% in the output structure in the column index. e.g. localDoppler.startPhase refers to a block of 1024 registers. If
% there are 4 sets of values, then localDoppler.startPhase will be a 1024x4 matrix.
%
% Registers set here:
%  lfaadecode100g.statctrl.vctable        : maximum 1024 entry virtual channel table
%  lfaadecode100g.statctrl.total_stations : total number of stations
%  lfaadecode100g.statctrl.total_coarse   : total number of coarse channels
%  lfaadecode100g.statctrl.total_channels : total number of virtual channels. Should be total_stations * total_coarse.
%  
%  ct_atomic_pst_in.config.table_0  :  Configures the coarse and fine delays.
%  ct_atomic_pst_in.config.table_1  :
%  
%  pstbeamformer.pstbeamformer.data : Jones matrices and phase tracking for the PST beamformer.
% ---------------------------------------------------------------------------------
% PSTconst
% --------
%
%  filterbanks.firtaps.data
%
%
%  

%% Registers for the filterbanks.
% The filter taps for the filterbanks are fixed for all FPGAs and have default values in the firmware, so they are stored in PSTconst instead of PSTreg.
% generate_MaxFlt creates filters with a DC response of 0.5
% i.e. sum(globalreg.PSSFilterbankTaps(X:64:end)) = 0.5
% The maximum value for the filter taps is about 0.55
% The firmware has 18 bits to represent the filter taps (i.e. range = -131072 to 131071).
% So scale by 2^17 (=131072)
%
% The generate_MaxFlt function doesn't work in Octave, so the data is saved to a file instead.
% The function "write_filter_taps.m" can be run in Matlab to generate the files
%  pss-filter.txt, pst-filter.txt, and correlator-filter.txt
% Note that there is an equivalent piece of code in the firmware repository :
%  low-cbf-firmware/libraries/signalProcessing/filterbanks/src/matlab/get_row_coefficients
% which generates the .coe files to initialise the memories used in the firmware for the filterbanks.
%
%
% globalreg.PSSFilterbankTaps = load('pss-filter.txt');
% globalreg.correlatorFilterbankTaps = load('correlator-filter.txt'); % Correlator, 4096 point FFT, 12 taps.
PSTconst.PSTFilterbankTaps = load('pst-filter.txt'); % PST, 256 point FFT, 12 taps

% Also find the location of the maximum of the filters, which is used for setting the fine delay (since fine delay changes with time).
mv = max(PSTconst.PSTFilterbankTaps);
mi = find(PSTconst.PSTFilterbankTaps == mv);
PSTconst.PSTFilterbankMaxIndex = median(mi);


%% Work out how many delay updates we need
if (modelConfig.delayUpdatePeriod == 0)
    updates = 1; % i.e. just the initial value.
else
    totalRuntime = modelConfig.runtime * 1080e-9 * 408 * 2048; 
    updates = floor(totalRuntime / modelConfig.delayUpdatePeriod) + 1;
    % Number of LFAA frames per update period
    updatePeriod = (totalRuntime / updates) / (1080e-9 * 2048);
end

registerCount = 0;  % Total number of different registers we have set.

%% Registers for LFAASPEADDecoder module
% .virtualChannel
% Each entry in the virtual channel table has 
%   bits(8:0) = frequency_id     (valid range 65 to 448)
%   bits(12:9) = beam_id         (valid range 1 to 8)
%   bits(15:13) = substation_id  (valid range 1 to 4)
%   bits(20:16) = subarray_id    (valid range 1 to 16)
%   bits(30:21) = station_id     (valid range 1 to 512)
%   bit(31) = '1' to indicates that this entry is invalid.
% For Atomic COTS operation:
%  Entries need to be sorted by station and channel
%  e.g. if we have 3 stations 5,10, and 12, and 2 channels, 100 and 101, then
%  they would be listed in the table entries 0 to 5:
%    0 - station 5, channel 100
%    1 - station 10, channel 100
%    2 - station 12, channel 100
%    3 - station 5, channel 101
%    4 - station 10, channel 101
%    5 - station 12, channel 101

virtualChannelTable = zeros(num_PSTBF_cores,1024,13);
channel_count = 0;

for pstbf_core_idx=1:num_PSTBF_cores
 
    registerCount = registerCount + 1;
    if(pstbf_core_idx~=1)
      PSTreg(registerCount).name = sprintf('lfaadecode100g_%d.statctrl.vctable',pstbf_core_idx);
    else
      PSTreg(registerCount).name = 'lfaadecode100g.statctrl.vctable';
    end
    PSTreg(registerCount).offset = 0;
    PSTreg(registerCount).value = zeros(1024,1,'uint32');
    % -----------------------------------------------------------------------
    % Warning - bit of a hack at present to allow for atomic COTs operation.
    % Does not account for multiple subarrays and station beams.
    % -----------------------------------------------------------------------
    
    b1 = 1;  % Assume we are just using the first station beam.
    subArray = 1; % Assume just one subarray.
    currentSubArray = 1;

    %distribute each station's coarse channels evenly to each PST BF core
    if length(modelConfig.channelList) > num_PSTBF_cores
       if pstbf_core_idx ~= num_PSTBF_cores
          channel_num = floor(length(modelConfig.channelList)/num_PSTBF_cores);
       else
          channel_num = length(modelConfig.channelList) - (num_PSTBF_cores-1)*floor(length(modelConfig.channelList)/num_PSTBF_cores);
       end
    elseif pstbf_core_idx > length(modelConfig.channelList) 
       continue; 
    else
       channel_num = 1;
    end

    virtualChannelCount = 0;
    
    for station = 0:(length(arrayConfigFull.stations(1).stationIDs) - 1)
        for channel = 1:channel_num
            % Columns in virtualChannelTable
            %  1 => frequency_id, 2 => beam_id, 3 => substation_id, 4 => subarray_id, 5 => station_id, 
            %  6 => index X into arrayConfigFull.stationBeam(X), 7 => index X into arrayConfigFull.subArray(X), 8 => logical ID, 9 => table entry is unused
            % 10 => LRU this stationID came in on.
            % 11 => Virtual Channel.
            virtualChannelCount = virtualChannelCount + 1;

            % These entries get combined to form the virtual channel table in the LFAASPEADDecoder module
            virtualChannelTable(pstbf_core_idx,virtualChannelCount,1) = modelConfig.channelList(channel_count+channel);      % was = arrayConfigFull.stationBeam(b1).channels(c1);
            virtualChannelTable(pstbf_core_idx,virtualChannelCount,2) = arrayConfigFull.stationBeam(b1).index;
            virtualChannelTable(pstbf_core_idx,virtualChannelCount,3) = 1; % substationIDs(subArray);
            virtualChannelTable(pstbf_core_idx,virtualChannelCount,4) = currentSubArray;
            virtualChannelTable(pstbf_core_idx,virtualChannelCount,5) = arrayConfigFull.stations(1).stationIDs(station+1);   % station_id;
            % The remaining entries are for convenience use later in this function.
            virtualChannelTable(pstbf_core_idx,virtualChannelCount,6) = b1;  % Just for convenience; used later in this function to look up the correct stationBeam parameters for this entry.
            virtualChannelTable(pstbf_core_idx,virtualChannelCount,7) = 1; %subArrayIndexes(subArray);  % Also just for convenience; used to look up the subarray this virtual channel is part of.
            virtualChannelTable(pstbf_core_idx,virtualChannelCount,8) = arrayConfigFull.stations(1).logicalIDs(station+1); % logicalIDs(subArray);  % logical ID of this station, can differ from the station id when there are substations.
            virtualChannelTable(pstbf_core_idx,virtualChannelCount,9) = 0; % this entry is not invalid.
            virtualChannelTable(pstbf_core_idx,virtualChannelCount,10) = 1;  % Unused.
            virtualChannelTable(pstbf_core_idx,virtualChannelCount,11) = virtualChannelCount-1;
            virtualChannelTable(pstbf_core_idx,virtualChannelCount,12) = channel;  % record the channel count and station count for this virtual channel
            virtualChannelTable(pstbf_core_idx,virtualChannelCount,13) = station;
        end
    end
    virtualChannelTemp = zeros(virtualChannelCount,1,'uint32');
    for vc = 1:virtualChannelCount
        virtualChannelTemp(vc) = uint32(virtualChannelTable(pstbf_core_idx,vc,1) + ...
                                        virtualChannelTable(pstbf_core_idx,vc,2) * 2^9 + ...
                                        virtualChannelTable(pstbf_core_idx,vc,3) * 2^13 + ...
                                        virtualChannelTable(pstbf_core_idx,vc,4) * 2^16 + ...
                                        virtualChannelTable(pstbf_core_idx,vc,5) * 2^21);
    end
    PSTreg(registerCount).value = virtualChannelTemp;
    channel_count = channel_count + channel_num;

    %% Other lfaadecode100g registers : total stations, total_coarse, total_channels
    registerCount = registerCount + 1;
    if(pstbf_core_idx~=1)
      PSTreg(registerCount).name = sprintf('lfaadecode100g_%d.statctrl.total_stations',pstbf_core_idx);
    else
      PSTreg(registerCount).name = 'lfaadecode100g.statctrl.total_stations';
    end
    PSTreg(registerCount).offset = 0;
    PSTreg(registerCount).value = length(arrayConfigFull.stations(1).stationIDs);

    registerCount = registerCount + 1;
    if(pstbf_core_idx~=1)
      PSTreg(registerCount).name = sprintf('lfaadecode100g_%d.statctrl.total_coarse',pstbf_core_idx);
    else  
      PSTreg(registerCount).name = 'lfaadecode100g.statctrl.total_coarse';
    end
    PSTreg(registerCount).offset = 0;
    PSTreg(registerCount).value = channel_num;

    registerCount = registerCount + 1;
    if(pstbf_core_idx~=1)
      PSTreg(registerCount).name = sprintf('lfaadecode100g_%d.statctrl.total_channels',pstbf_core_idx);
    else  
      PSTreg(registerCount).name = 'lfaadecode100g.statctrl.total_channels';
    end
    PSTreg(registerCount).offset = 0;
    PSTreg(registerCount).value = length(arrayConfigFull.stations(1).stationIDs)*channel_num;


    %% Input Corner Turn
    %  Delay for each coarse channel, based on the virtual channel number.
    %  
    %  The table in the corner turn has 2 x(32bit words) per virtual channel. 
    %  Each pair of words has the following fields:
    %    word 0 : bits 10:0 = coarse delay
    %             bits 31:16 = H pol delta phase = phase step across the coarse channel (see confluence pages for details)
    %    word 1 : bits 15:0 = V pol delta phase = phase step across the coarse channel 
    %             bits 31:16 = delta delta phase (common to both polarisations)
    %
    %

    registerCount = registerCount + 1;
    if(pstbf_core_idx~=1)
      PSTreg(registerCount).name = sprintf('ct_atomic_pst_in_%d.config.table_0',pstbf_core_idx);
    else  
      PSTreg(registerCount).name = 'ct_atomic_pst_in.config.table_0';
    end
    PSTreg(registerCount).offset = 0;
    PSTreg(registerCount).value = zeros(virtualChannelCount*4,ceil(updates/2));  % Every second update goes to table_0, every other update goes to table_1.

    if(updates > 1)
      if(pstbf_core_idx~=1)
        PSTreg(registerCount+1).name = sprintf('ct_atomic_pst_in_%d.config.table_1',pstbf_core_idx);
      else
        PSTreg(registerCount+1).name = 'ct_atomic_pst_in.config.table_1';
      end
      PSTreg(registerCount+1).offset = 0;
      PSTreg(registerCount+1).value = zeros(virtualChannelCount*4,floor(updates/2));  % Every second update goes to table_0, every other update goes to table_1.
    end

    if(pstbf_core_idx~=1)
      PSTreg(registerCount+2).name = sprintf('ct_atomic_pst_in_%d.config.table0_startpacket',pstbf_core_idx);  
    else
      PSTreg(registerCount+2).name = 'ct_atomic_pst_in.config.table0_startpacket';
    end
    PSTreg(registerCount+2).offset = 0;
    PSTreg(registerCount+2).value = 0;  % The packet count at which we start using the data in table_0

    if(pstbf_core_idx~=1)
      PSTreg(registerCount+3).name = sprintf('ct_atomic_pst_in_%d.config.table1_startpacket',pstbf_core_idx);
    else
      PSTreg(registerCount+3).name = 'ct_atomic_pst_in.config.table1_startpacket';
    end
    PSTreg(registerCount+3).offset = 0;
    PSTreg(registerCount+3).value = 0;  % The packet count at which we start using the data in table_1
    for frame = 1:updates
        if frame == 1
           PSTreg(registerCount+2).value(frame) = 0;   % use table_0 from the start (i.e. frame 0)
           PSTreg(registerCount+3).value(frame) = round(frame * updatePeriod);   % table_1 hasn't been loaded yet, but when it does, it will be used from this packet count.
        else
           PSTreg(registerCount+2).value(frame) = round(floor((frame-1)/2)*2 * updatePeriod);        % i.e. 0, 0, 2, 2, 4, 4, 6, 6, etc.
           PSTreg(registerCount+3).value(frame) = round((ceil((frame-1)/2)*2 - 1) * updatePeriod);   % i.e. 0, 1, 1, 
        end
    end

    % as currently only one station beam is available for all the pipelines, so actually for all the pipelines, all delay tables are same  
    for virtualChannel = 1:virtualChannelCount
        % Get the delay polynomial for this virtual channel
        logicalIDIndex = find(arrayConfigFull.subArray(virtualChannelTable(pstbf_core_idx,virtualChannel,7)).logicalIDs == virtualChannelTable(pstbf_core_idx,virtualChannel,8));
    
        delayPoly = arrayConfigFull.stationBeam(virtualChannelTable(pstbf_core_idx,virtualChannel,6)).delayPolynomial(logicalIDIndex,:);  % The delay polynomial for this virtual channel 
        HVOffset  = arrayConfigFull.stationBeam(virtualChannelTable(pstbf_core_idx,virtualChannel,6)).HVOffset(logicalIDIndex);

        for frame = 1:updates
            currentTime = (frame - 1) * 1080e-9 * 2048 * 408;
            % Evaluate the delay polynomial and its derivative
            delaySeconds = delayPoly(1) * currentTime^3 + delayPoly(2) * currentTime^2 + delayPoly(3) * currentTime + delayPoly(4);
            DdelayDt = 3 * delayPoly(1) * currentTime^2 + 2 * delayPoly(2) * currentTime + delayPoly(3);
            delaySamples = round(delaySeconds/(1080e-9));
        
            delayFracH = delaySeconds - delaySamples * 1080e-9; % in seconds
            delayFracV = delayFracH + HVOffset;
            % Fine delay uses the Phase change across one coarse channel.
            % See the coarse corner turn confluence page for an explanation.
            deltaPH = round((1/1080e-9) * (1/2) * delayFracH/(2^(-12)) * 2^3);  
            deltaPV = round((1/1080e-9) * (1/2) * delayFracV/(2^(-12)) * 2^3);
            deltadeltaP = round((1/1080e-9) * (1/2) * (DdelayDt * 64*1080e-9)/(2^(-15)) * 2^15);
        
            % The input corner turn also needs to include a phase offset and phase offset step per time
            skyFrequency = virtualChannelTable(pstbf_core_idx,virtualChannel,1) * (27/32)*(1/1080e-9);  % sky frequency in Hz for the center of the band
        
            % To correctly implement the coarse delay, we need to include a phase offset.
            % Even if the required delay is an integer number of LFAA samples, we still need a phase offset 
            % because there is not an integer number of local oscillator clock periods in the 1080ns LFAA sample period.
            % (The non-integer relationship is cause by the 32/27 LFAA oversampling.)
            OffsetPhaseH = delaySeconds * skyFrequency;  % i.e. delay/skyPeriod. This value is in units of rotations (i.e. basePhase = 1 --> phase of 2*pi)
            OffsetPhaseV = (delaySeconds + HVOffset) * skyFrequency;
        
            offsetPH = mod(round(2^16 * mod(OffsetPhaseH,1)),2^16);
            offsetPV = mod(round(2^16 * mod(OffsetPhaseV,1)),2^16);
        
            deltaDelay = DdelayDt * 64 * 1080e-9;  % The delay offset is updated every 64 coarse channel time samples.
            phaseBaseTimeStep = deltaDelay * skyFrequency;
            offsetPStep = 2^32 * mod(phaseBaseTimeStep,1);
        
            if ((delaySamples < 1) || (delaySamples > 2047))
               error(['Calculated Coarse Delay is ' num2str(delaySamples) ' LFAA samples. It must be between 1 and 2047']);
            end
            % Two words in the table for each channel and station.
            % Updates alternate between "ct_atomic_pst_in.config.table_0" and "ct_atomic_pst_in.config.table_1"
            if(mod(frame,2) == 1)
              PSTreg(registerCount).value(4*(virtualChannel-1) + 1,ceil(frame/2)) = 2^16 * TC16(deltaPH) + TC16(2048 - delaySamples);  % !!! Note : Large Delay means smaller read start address, hence (2048 - delaySamples)
              PSTreg(registerCount).value(4*(virtualChannel-1) + 2,ceil(frame/2)) = 2^16 * TC16(deltadeltaP) + TC16(deltaPV);
              PSTreg(registerCount).value(4*(virtualChannel-1) + 3,ceil(frame/2)) = 2^16 * (offsetPV) + (offsetPH);
              PSTreg(registerCount).value(4*(virtualChannel-1) + 4,ceil(frame/2)) = mod(round(offsetPStep),2^32);
            else
              PSTreg(registerCount+1).value(4*(virtualChannel-1) + 1,floor(frame/2)) = 2^16 * TC16(deltaPH) + TC16(2048 - delaySamples);
              PSTreg(registerCount+1).value(4*(virtualChannel-1) + 2,floor(frame/2)) = 2^16 * TC16(deltadeltaP) + TC16(deltaPV);
              PSTreg(registerCount+1).value(4*(virtualChannel-1) + 3,ceil(frame/2)) = 2^16 * (offsetPV) + (offsetPH);
              PSTreg(registerCount+1).value(4*(virtualChannel-1) + 4,ceil(frame/2)) = mod(round(offsetPStep),2^32);
            end
        end
    end

    registerCount = registerCount + 3;  % Since there are 3 extra registers set above.

    % Reset the corner turn
    registerCount = registerCount + 1;
    if(pstbf_core_idx~=1)
      PSTreg(registerCount).name = sprintf('ct_atomic_pst_in_%d.config.full_reset',pstbf_core_idx);
    else
      PSTreg(registerCount).name = 'ct_atomic_pst_in.config.full_reset';
    end
    PSTreg(registerCount).offset = 0;
    PSTreg(registerCount).value = 1;

    registerCount = registerCount + 1;
    if(pstbf_core_idx~=1)
      PSTreg(registerCount).name = sprintf('ct_atomic_pst_in_%d.config.full_reset',pstbf_core_idx);
    else
      PSTreg(registerCount).name = 'ct_atomic_pst_in.config.full_reset';
    end
    PSTreg(registerCount).offset = 0;
    PSTreg(registerCount).value = 0;

    %% Registers for PST beamforming
    %  Currently replicate the PST beam for each pipeline until another PSTbeam format for different pipeline is defined 
    for beam = 1:length(arrayConfigFull.PSTBeam)
        % Jones matrices 
        % Note that Jones is a 4D array, with dimensions (station, channel, 2,2)
        jones = arrayConfigFull.PSTBeam(beam).jonesReal + 1i * arrayConfigFull.PSTBeam(beam).jonesImag;
    
        weights = arrayConfigFull.PSTBeam(beam).weights;
    
        registerCount = registerCount + 1;
        if(pstbf_core_idx~=1)
          PSTreg(registerCount).name = sprintf('pstbeamformer_%d.pstbeamformer.data',pstbf_core_idx);
        else  
          PSTreg(registerCount).name = 'pstbeamformer.pstbeamformer.data';
        end
        PSTreg(registerCount).offset = (beam-1)*(65536/4);  % 64k bytes per beamformer, divide by 4 for word addressing.
        PSTreg(registerCount).value = zeros(virtualChannelCount*4,1);  % 4 x 32 bit words per Jones matrix.
    
        for vc = 1:virtualChannelCount
            channel = virtualChannelTable(pstbf_core_idx,vc,12);
            station = virtualChannelTable(pstbf_core_idx,vc,13)+1;  % convert from 0 to 1 based.
            jones2x2 = weights(station) * shiftdim(jones(station,channel,:,:));
            jones2x2_16bit = round(32767 * jones2x2);
            % Convert to 16 bit values and put in the correct places in the registers.
            PSTreg(registerCount).value((vc-1)*4+1) = TC16(real(jones2x2_16bit(1,1))) + 65536 * TC16(imag(jones2x2_16bit(1,1)));
            PSTreg(registerCount).value((vc-1)*4+2) = TC16(real(jones2x2_16bit(1,2))) + 65536 * TC16(imag(jones2x2_16bit(1,2)));
            PSTreg(registerCount).value((vc-1)*4+3) = TC16(real(jones2x2_16bit(2,1))) + 65536 * TC16(imag(jones2x2_16bit(2,1)));
            PSTreg(registerCount).value((vc-1)*4+4) = TC16(real(jones2x2_16bit(2,2))) + 65536 * TC16(imag(jones2x2_16bit(2,2)));
        end
    
        % Phase tracking
        registerCount = registerCount + 1;
        if(pstbf_core_idx~=1)
          PSTreg(registerCount).name = sprintf('pstbeamformer_%d.pstbeamformer.data',pstbf_core_idx);
        else  
          PSTreg(registerCount).name = 'pstbeamformer.pstbeamformer.data';
        end
        PSTreg(registerCount).offset = (beam-1)*(65536/4) + (32768/4);  % 64k bytes per beamformer, divide by 4 for word addressing. Phase tracking info is in the second 32k bytes.
        PSTreg(registerCount).value = zeros(virtualChannelCount*4,1);   % 4 x 32 bit words of phase info per virtual channel.
        for vc = 1:virtualChannelCount
        
            delayPoly = arrayConfigFull.PSTBeam(beam).delayPolynomial(virtualChannelTable(pstbf_core_idx,vc,13) + 1,:);  % The delay polynomial for this virtual channel         
        
            frame = 1;  % Just a single update of the registers at the moment.
            currentTime = (frame - 1) * 1080e-9 * 2048 * 408;
            % Evaluate the delay polynomial and its derivative for the PST beam
            delaySeconds = delayPoly(1) * currentTime^3 + delayPoly(2) * currentTime^2 + delayPoly(3) * currentTime + delayPoly(4);
            DdelayDt = 3 * delayPoly(1) * currentTime^2 + 2 * delayPoly(2) * currentTime + delayPoly(3);
            deltaDelay = DdelayDt * 192 * 1080e-9; % 192 LFAA samples per time step for the output of the PST filterbanks.
        
            % 
            % To convert delay to phase we need to divide by the period of the signal at the sky frequency.
            % Since F=1/T, delay divided by period is delay times frequency, and the phase difference we need to apply across the band is 
            % only dependent on the bandwidth. However absolute phase depends on the sky frequency.
            %
            % PST uses a 256 point FFT in the filterbank, and retains the central 27/32 * 256 = 216 fine channels.
            % LFAA sample period is 1080 ns, so the step between PST fine channels is (1/1080e-9)/256 = 3.617 kHz
            % So the frequency at the bottom of the band is (216/2) * ((1/1080e-9)/256)
            % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            % Check where the center frequency is meant to be located for PST (1/2 channel offset or not ?)
            % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            % 
            skyFrequency = virtualChannelTable(pstbf_core_idx,vc,1) * (27/32)*(1/1080e-9);  % sky frequency in Hz for the center of the band
            skyFrequencyStep = (1/1080e-9)/256;
            skyFrequencyLow = skyFrequency - (216/2)*skyFrequencyStep; % sky frequency for the first PST fine channel 
        
            % Get the firmware parameters
            basePhase = delaySeconds * skyFrequencyLow;  % i.e. delay/skyPeriod. This value is in units of rotations (i.e. basePhase = 1 --> phase of 2*pi)
            phaseFreqStep3channels = 3 * skyFrequencyStep * delaySeconds;
            phaseBaseTimeStep = deltaDelay * skyFrequencyLow; % deltaDelay is the extra delay needed for each filterbank output timestep.
            phaseFreqStep3channelsTimeStep = 3 * skyFrequencyStep * deltaDelay;
            % Drop whole rotations from the phase parameters so they can be represented as 32 bit unsigned values for the firmware.
            % In the firmware, 2^32 = 1 rotation.
            basePhaseMod = 2^32 * mod(basePhase,1);
            phaseFreqStep3channelsMod = 2^32 * mod(phaseFreqStep3channels,1);
            phaseBaseTimeStepMod = 2^32 * mod(phaseBaseTimeStep,1);
            phaseFreqStep3channelsTimeStepMod = 2^32 * mod(phaseFreqStep3channelsTimeStep,1);
        
            % The firmware has no register for "phaseFreqStep3channelsTimeStep", the change in the phase step across frequency with time.
            % This value should be small.
            % Check and give a warning if it is not.
            if (phaseFreqStep3channelsTimeStepMod > 2^31)
               t1 = phaseFreqStep3channelsTimeStepMod - 2^32;
            else
               t1 = phaseFreqStep3channelsTimeStepMod;
            end
            if ((t1 > 1) || (t1 < -1))
               warning(['Phase adjustment per time step for the frequency step in the PST beamformer is larger than expected (' num2str(t1) ')']);
            end

            % 4 x 32 bit words per phase update.
            PSTreg(registerCount).value((vc-1)*4 + 1) = mod(round(basePhaseMod),2^32);  % base phase, 32 bits.
            PSTreg(registerCount).value((vc-1)*4 + 2) = mod(round(phaseFreqStep3channelsMod),2^32);  % phase step per 3 PST fine channels. 32 bits.
            PSTreg(registerCount).value((vc-1)*4 + 3) = mod(round(phaseBaseTimeStepMod),2^32);  % phase step per time sample. 32 bits.
            PSTreg(registerCount).value((vc-1)*4 + 4) = round(65536 * arrayConfigFull.PSTBeam(beam).weights(virtualChannelTable(pstbf_core_idx,vc,13) + 1));  % 16 bit weight for this station
        
        end
    end

    % Enable the number of configured beams.
    registerCount = registerCount + 1;

    if(pstbf_core_idx~=1)
      PSTreg(registerCount).name = sprintf('ct_atomic_pst_out_%d.statctrl.beamsenabled',pstbf_core_idx);
    else
      PSTreg(registerCount).name = 'ct_atomic_pst_out.statctrl.beamsenabled';
    end
    PSTreg(registerCount).offset = 0;
    PSTreg(registerCount).value = length(arrayConfigFull.PSTBeam);

end

% Enable the packetiser
registerCount = registerCount + 1;
PSTreg(registerCount).name = 'packetiser.packetiser_ctrl.control_vector';
PSTreg(registerCount).offset = 0;
PSTreg(registerCount).value = 9; %set the value to 9, so the output packet header will contain the meta data such as dst src IP address etc


if (1==0) % Need to update this for atomic cots
%% Registers for PSS beamforming
% Each PSS beam has a set of complex weights for the stations involved.
% up to 512 stations contribute to each beam. 
% For each beam we need a complex weight for each station and each fine channel.
% So the maximum is (500 beams) * (512 stations) * (54*384 fine channels) * (2bytes/weight (at least)) / (288 LRUs) = 36 Mbytes of weights per FPGA
% Obviously that won't work.
% Instead, we have a phase slope across the entire (300MHz) band - same data needs to be stored for all LRUs, so
% (500 beams) * (512 stations) * (4 bytes/weight at least - 2 for slope + 2 for linear change in slope) = 1 Mbyte = 32 URAMs.
%  Data required for each station and beam :
%   * Phase slope
%   * Linear change in the phase slope
%   * valid - i.e. If this station contributes to this beam or not.
% We also need to specify which stations and coarse channels contribute to each beam - do this with a bit vector for each beam and each LRU
% For AA4, each LRU has 72 fine channels from all 512 stations (9 fine channels from each of 8 coarse channels).
%
% Registers (Same for all LRUs)
%  globalreg.PSSPhase : array 512x500xT (station x beam x updates), with phase across a single 14.49kHz channel, in units of 2^(-25) rotations. Signed 24 bit value.
%  globalreg.PSSPhaseDelta : array 512x500xT (station x beam x updates), with the phase change delta per 64 LFAA samples, in units of 2^(-42) rotations. Signed 24 bit value.
%  globalreg.PSSUsed : array 512x500 (station x beam), indicates if the station is used for the beam.
globalreg.PSSPhase = zeros(512,500,modelConfig.runtime,'int32');
globalreg.PSSPhaseDelta = zeros(512,500,modelConfig.runtime,'int32');
globalreg.PSSUsed = zeros(512,500,'int32');
% Get a list of stations (indexed by stationIDs, not logicalIDs)
stationList = unique(arrayConfigFull.stationsFull.stationIDs);
for stationIndex = 1:length(stationList)
    stationID = stationList(stationIndex);

    % Step through the PSS beams
    if isfield(arrayConfigFull,'PSSBeam')
        NBeams = length(arrayConfigFull.PSSBeam);
    else
        NBeams = 0;
        disp('No PSS beams defined; skipping register generation for PSS beams');
    end
    for beam = 1:NBeams
        % -------------
        % Work out if this beam uses this stationID, and if so, which polynomial it is.
        % Get the list of logical IDs for the stations in the subarray for this beam.
        logicalIDs = arrayConfigFull.subArray(arrayConfigFull.PSSBeam(beam).subArray).logicalIDs;
        % Check if any of these logicalIDs match this stationID
        matchesFound = 0;
        for i1 = 1:length(logicalIDs)
            if (stationID == arrayConfigFull.stationsFull.stationIDs(find(arrayConfigFull.stationsFull.logicalIDs == logicalIDs(i1))))
                matchesFound = matchesFound + 1;
                matchIndex = i1;
            end
        end
        if (matchesFound > 1)
            error('More than one matching logical ID for the station ID in PSS beamforming');
        end
        % -------------
        if (matchesFound == 1)
            % Get the Polynomial
            delayPoly = arrayConfigFull.PSSBeam(beam).delayPolynomial(matchIndex,:);
            % Evaluate the polynomial and its derivative at the PSS update interval
            % The PSS update interval is currently set here to once per 0.9 second frame - in the real system this is more likely to be about once per 10 seconds.
            for frame = 1:modelConfig.runtime
                currentTime = (frame - 1) * 1080e-9 * 2048 * 408;
                delaySeconds = delayPoly(1) * currentTime^3 + delayPoly(2) * currentTime^2 + delayPoly(3) * currentTime + delayPoly(4);
                DdelayDt = 3 * delayPoly(1) * currentTime^2 + 2 * delayPoly(2) * currentTime + delayPoly(3);
                phaseRegister = ((1/1080e-9)/64) * delaySeconds / (2^(-25));   % Phase across one PSS fine channel, in units of 2^(-25) rotations.
                deltaPhaseRegister = ((1/1080e-9)/64) * DdelayDt * 64 * 1080e-9 / (2^(-42)); % phase step, units of 2^(-42) rotations per 64 LFAA samples.
                % Check values are reasonable
                if ((abs(phaseRegister) > 2^23) || (abs(deltaPhaseRegister) > 2^23))
                    warning('PSS phase corrections do not fit in 3 bytes');
                end
                globalreg.PSSPhase(stationID,beam,frame) = round(phaseRegister);
                globalreg.PSSPhaseDelta(stationID,beam,frame) = round(deltaPhaseRegister);
                globalreg.PSSUsed(stationID,beam) = 1;
                % Check the error in the linear approximation from the first to the last frame
                if (frame > 1)
                    predictedPhase = double(globalreg.PSSPhase(stationID,beam,1)) + 2^(-17) * double(globalreg.PSSPhaseDelta(stationID,beam,1)) * (frame - 1) * 2048 * 408 / 64; 
                    actualPhase = globalreg.PSSPhase(stationID,beam,frame);
                    if ((predictedPhase - actualPhase) > 2)
                        warning('Linear prediction for PSS phase is poor');
                        keyboard
                    end
                end
                %keyboard
            end
        else
            % No match found, i.e. this station does not contribute to this beam.
            globalreg.PSSPhase(stationID,beam,:) = 0;
            globalreg.PSSPhaseDelta(stationID,beam,:) = 0;
            globalreg.PSSUsed(stationID,beam) = 0;
        end
    end
end
end



