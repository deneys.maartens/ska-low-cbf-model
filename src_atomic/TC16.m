function [d16] = TC16(din)
% Convert input data to a 16 bit two's complement integer
if (din < -32768)
    d1 = -32768;
elseif (din > 32767)
    d1 = 32767;
else
    d1 = din;
end

if (d1 < 0)
    d1 = d1 + 65536;
end

d16 = round(d1);


