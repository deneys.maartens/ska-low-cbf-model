## Low CBF Model

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-low-cbf-model/badge/?version=latest)](https://ska-telescope-low-cbf-model.readthedocs.io/en/latest/?badge=latest)

This repository contains a Matlab model of signal processing performed by Low-CBF FPGA firmware. The model can be run using either Matlab or Octave.

## About

Low-CBF FPGAs receive UDP data packets containing station data from LFAA stations. They filter, beamform, and cross-correlate the data and send output to SDP. The model:
* Simulates the sky and LFAA stations to generate synthetic data packets that LFAA stations would produce
* Processes the packets exactly as the FPGA implemenation of Low-CBF does
* Generates outputs and intermediate results that can be compared with real FPGA outputs

There is a related tool (lfaa-sim) that is able to take packet output from this model and play the packets over a 40GbE network. This allows an FPGA implementing Low-CBF processing to be tested in real-time by capturing the outputs that occur in response to specific simulated circumstances and comparing them with the model's intermediate results and outputs. See <https://github.com/ska-telescope/low-cbf-tools>

## Documentation
Documentation of the model, and a 'howto' describing its use can be found here: <https://confluence.skatelescope.org/display/SE/Packetised+Model+Overview>

## Dependencies
This model is known to run using either
* Matlab R2018
* Octave 4.0.0
When using Octave, the jsonlab package from Github is an additional dependency, as specified in the Documentation. Version Jsonlab 1.9 has been used successfully. Later versions of these tools are also likely to work.

# Matlab Quick Commands
Navigate to the root of the GIT repo in matlab.

* create_config('run2', 1, number_of_PST_Pipelines)

That will make the matlab model. The number of number_of_PST_Pipelines is either 1 or 3. 

* get_LFAA_raw('run2')

This will create the two RAW files that are further converted to PCAP. Also the register settings for PST in the "tb" directory of the run.

# Python Module

The python module 'ska-low-cbf-model' at present contains only a utility for
converting the model-produced "raw" packet format into pacp.

As the `src` directory was already used, the Python code resides in `python`.
This is non-standard but seemed like the most sensible choice for now.

### RAW to PCAP

Given a RAW header file and data file, produces a PCAP file.

```
usage: raw_to_pcap [-h] headers data output

RAW to PCAP

positional arguments:
  headers     RAW header file
  data        RAW data file
  output      Output PCAP(NG) file

optional arguments:
  -h, --help  show this help message and exit
```

# Changelog

### 0.1.2
* Changes in SPEAD initialisation and data scapy-based packet definitions
  * init packet has all the static configurations for a LOW CBF to SDP transmission. This means it contains all the descriptors from the ICD except Time and visibility data
  * data packet only contains Time and visibility data

### 0.1.1
* Correlator model with SPEAD packet generator.

### 0.0.1
* Original model able to analyse the matlab output.