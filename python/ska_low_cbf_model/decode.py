# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
# All rights reserved

import dpkt
import numpy as np

from ska_low_cbf_model import lfaa_spead
from ska_low_cbf_model.lfaa_spead import complex_int8, lfaa_spead_header


def check_lfaa_header(header: lfaa_spead_header, payload: bytes):
    """
    :param header: the LFAA SPEAD header to check
    :param payload: the LFAA SPEAD payload (to check length matches header)
    :raises AssertionError: If any check fails
    Returns the condensed header information used in the virtual channel table.
    """

    assert header["magic"] == 0x53, "SPEAD magic number is wrong"
    assert header["version"] == 4, "SPEAD version should be 4"
    assert (
        header["item_pointer_width"] == 2
    ), "SPEAD item pointer width field should be 2"
    assert header["N_items"] == 8, "SPEAD number of header items should be 8"
    assert len(payload) == 8192, "SPEAD payload should be 8192 bytes"
    # could also validate/decode other LFAA SPEAD header fields

    # Get the fields that are used to lookup the virtual channel.
    #  bits 2:0 = substation_id,
    #  bits 12:3 = station_id,
    #  bits 16:13 = beam_id,
    #  bits 25:17 = frequency_id
    #  bits 30:26 = subarray_id
    #  bit  31 = set to '1' to indicate this entry is invalid
    assert (
        header["csp_substation_id"] < 8
    ), "SPEAD substation must be less than 8"
    vc_lookup = int(header["csp_substation_id"])
    assert (
        header["csp_station_id"] < 1024
    ), "SPEAD station id must be less than 1024"
    vc_lookup += int(header["csp_station_id"]) * (2**3)
    assert (
        header["csp_channel_beam_id"] < 16
    ), "SPEAD beam id must be less than 16"
    vc_lookup += int(header["csp_channel_beam_id"]) * (2**13)
    assert (
        header["csp_channel_frequency_id"] < 512
    ), "SPEAD frequency id must be less than 512"
    vc_lookup += int(header["csp_channel_frequency_id"]) * (2**17)
    assert (
        header["csp_subarray_id"] < 32
    ), "SPEAD subarray id must be less than 32"
    vc_lookup += int(header["csp_subarray_id"]) * (2**26)
    return vc_lookup


def decode_ethernet(packets, vc_table) -> np.ndarray:
    """
    Decodes ethernet packets for signal processing.

    :param packets: an iterable, each element representing one packet
    :param vc_table: virtual channel table.
    :return: array[virtual_channel][polarisation][time], each element an LFAA
    sample.
    """
    print(f"📨 Decoding {len(packets)} ethernet packets")
    # pre-allocate array to hold ALL sample values
    # Extract the total number of virtual channels from vc_table.
    # Every second element of vc_table is a virtual channel.
    # Virtual channels are only valid if the associated channel info in the
    # previous word does not have the top bit set.
    max_vc = 0
    vc_used = np.zeros(2048)
    for vc_entry in range(0, vc_table.size, 2):
        # top bit in the vc_table indicates that the entry is unused.
        if (vc_table[vc_entry] & 0x80000000) == 0:
            this_vc = vc_table[vc_entry + 1]
            vc_used[this_vc] = 1
            if this_vc > max_vc:
                max_vc = this_vc

    total_vc = np.sum(vc_used)
    # Every second entry in the VC table,
    # i.e. the entries with info about the packets
    vc_table_lookup = vc_table[0::2]
    # odd indexed entries in the VC table, i.e. the virtual channel numbers.
    vc_table_vc = vc_table[1::2]
    # Assume all the virtual channels have the same amount of time in the input
    # data. Use the total number of packets in the input to determine how many
    # packets there are per virtual channel.
    # 2048 time samples per packet.
    total_time_samples = int(2048 * np.ceil(len(packets) / total_vc))
    # virtual channel runs from 0 to max_vc inclusive,
    # so the array size needs to be max_vc+1
    samples = np.zeros(
        (max_vc + 1, 2, total_time_samples), dtype=lfaa_spead.complex_int8
    )

    # Array to keep track of the number of packets for each virtual channel.
    packet_count = np.zeros((1024), dtype=int)
    # Each packet is 2048 samples x 2 polarisations
    packet_samples_shape = (lfaa_spead.SAMPLES_PER_PACKET, 2)

    skipped_count = 0
    total_packets_processed = 0
    for packet in packets:
        # raw returns a tuple for each packet,
        # with the list of bytes and the time it is meant to be sent.
        eth = dpkt.ethernet.Ethernet(bytes(packet[0]))
        if (
            eth.type == dpkt.ethernet.ETH_TYPE_IP
            and eth.data.p == dpkt.ip.IP_PROTO_UDP
        ):
            # TODO: should we check that the packet destination address matches
            #  our address?
            # ethernet -> IP -> UDP -> UDP payload
            udp_payload = eth.data.data.data
            header = np.frombuffer(
                udp_payload[: lfaa_spead_header.itemsize],
                dtype=lfaa_spead_header,
            )
            # get the rest of the packet from the end of the spead header on.
            lfaa_payload = udp_payload[lfaa_spead_header.itemsize :]
            vc_lookup = check_lfaa_header(header, lfaa_payload)
            vc_index = np.argwhere(vc_table_lookup == vc_lookup)
            this_packet_count = int(header["packet_counter"])
            if len(vc_index) == 1:
                # Found the matching virtual channel
                this_vc = int(vc_table_vc[vc_index])
                # Decode into data
                packet_samples = (
                    np.frombuffer(lfaa_payload, dtype=complex_int8)
                    .reshape(packet_samples_shape)
                    .T
                )
                # Put into the full data array, assuming that the packet count
                # starts from 0 in the incoming data stream.
                samples[
                    this_vc,
                    :,
                    this_packet_count
                    * 2048 : (this_packet_count * 2048 + 2048),
                ] = packet_samples

                packet_count[this_vc] += 1
                total_packets_processed += 1
            else:
                print("No matching virtual channel!")
        else:
            skipped_count += 1
    print(f"\tDecoded {np.sum(packet_count)} packets, skipped {skipped_count}")
    print(
        f"\tTotal time samples = {total_time_samples}, corresponding to"
        f" {total_time_samples * 1080e-9} seconds of data"
    )
    return samples
