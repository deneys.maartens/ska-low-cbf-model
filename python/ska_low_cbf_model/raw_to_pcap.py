# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
""" Convert RAW format files to PCAP """
import argparse
import os

import dpkt
import numpy as np

from ska_low_cbf_model import raw


def create_argument_parser() -> argparse.ArgumentParser:
    """Configure command-line"""
    parser = argparse.ArgumentParser(description="RAW to PCAP")
    parser.add_argument(
        "headers", type=argparse.FileType("rb"), help="RAW header file"
    )
    parser.add_argument(
        "data", type=argparse.FileType("rb"), help="RAW data file"
    )
    parser.add_argument(
        "output", type=argparse.FileType("wb"), help="Output PCAP(NG) file"
    )
    return parser


def main():
    """RAW -> PCAP Main Function"""
    parser = create_argument_parser()
    args = parser.parse_args()

    headers = np.frombuffer(args.headers.read(), dtype=raw.raw_header)
    data = np.frombuffer(args.data.read(), dtype=np.uint8)

    # parse input files
    raw_packets = raw.RawDataParser(headers, data)

    # write packets
    if os.path.splitext(args.output.name)[1] == ".pcapng":
        writer = dpkt.pcapng.Writer(args.output)
    else:
        writer = dpkt.pcap.Writer(args.output, nano=True)

    n_packets = 0
    for packet, timestamp in raw_packets:
        writer.writepkt(packet, timestamp)
        n_packets += 1

    print(f"Wrote {n_packets} packets to {args.output.name}")


if __name__ == "__main__":
    main()
