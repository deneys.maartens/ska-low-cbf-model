# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

"""
Read the "RAW" Ethernet packet format.
"""
import numpy as np

raw_header = np.dtype(
    [
        ("data_offset", ">u8"),
        ("bytes", ">u4"),
        (
            "time",
            ">u8",
        ),  # time, in ns, to send the packet. Relative to the first packet.
        ("reserved", "B", 12),
        ("ethernet_header", "B", 14),
        ("ipv4_header", "B", 20),
        ("udp_header", "B", 8),
        ("spead_header", "B", 72),
        ("unused_alignment", "B", 2),
    ]
)


ZERO_DATA_OFFSET = 0xFFFFFFFFFFFFFFFF


class RawDataParser:
    """RAW Decoder Class"""

    def __init__(self, headers: np.ndarray, data: np.ndarray):
        self._headers = headers
        self._data = data
        self._iter_index = 0

    def __getitem__(self, item: int) -> (np.ndarray, np.ndarray):
        header = self._headers[item]
        data_offset = header["data_offset"]
        data_bytes = header["bytes"]
        if data_offset == ZERO_DATA_OFFSET:
            data = np.zeros(data_bytes, dtype=np.uint8)
        elif data_offset >= len(self._data):
            raise ValueError("Header offset exceeds size of data")
        else:
            data = self._data[data_offset : (data_offset + data_bytes)]
        return np.concatenate(
            (
                header["ethernet_header"],
                header["ipv4_header"],
                header["udp_header"],
                header["spead_header"],
                data,
            )
        ), (header["time"] / 1e9)

    def __iter__(self):
        self._iter_index = -1
        return self

    def __next__(self):
        self._iter_index += 1
        if self._iter_index >= len(self._headers):
            raise StopIteration
        return self[self._iter_index]

    def __len__(self):
        return len(self._headers)
